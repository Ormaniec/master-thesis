\chapter{Teoria} \label{ch:chapter-02}

Algorytm \texttt{A*} zmniejszył złożoność obliczeniową, kosztem możliwego nieznalezienia najlepszej ścieżki (nie sprawdza
wszystkich ścieżek dostępnych w grafie). Pewne jest jednak, że zawsze znajdzie rozwiązanie (jeżeli jest osiągalne).
Algorytm realizowany w tej pracy skupi się na zmniejszeniu złożoności obliczeniowej, kosztem zwiększonego zużycia
pamięci.

\section{Zamysł} \label{subsec:chapter-02:idea}

Zastanawiając się nad sposobem rozwiązywania tego problemu, można zauważyć pewną zależność. Rozważmy problem przejazdu z
\emph{Legnicy} do \emph{Wrocławia}. Mamy dwie najbardziej opłacalne trasy. Jedna wykorzystuje drogę krajową, a
alternatywna autostradę. Jednoznaczne opisanie ścieżki wybranej wymaga podania kolejnych nazwy miejscowości.
Przykładowo:\newline

\texttt{ Legnica -> Prochowice -> Środa Śląska -> Wróblowice -> Wrocław}\newline
\texttt{ Legnica -> Szymanowice -> Budziszów W. -> Kąty Wrocławskie -> Wrocław }\newline

W ten sposób zostały opisane dwie szczególne ścieżki, mimo że nie użyliśmy numeru drogi czy współrzędnych. Z tak małą
ilością informacji jesteśmy w stanie sami zaplanować (z pamięci, lub spoglądając na mapę) trasę. Oczywiście,
prawdopodobnie istnieje krótsza trasa, która prowadzi pomiędzy podanymi dwoma lokacjami, jednak nie jesteśmy w stanie
tego stwierdzić, dopóki nie wykonamy dokładnej analizy mapy.

\section{Symulacja ludzkiego zachowania} \label{subsec:chapter-02:idea:simulation}
Trzeba zastanowić się w jaki sposób przenieść logikę, na implementację superszybkiego algorytmu planowania ścieżki.
Pierwszym pytaniem jest, jak powinna wyglądać mapa, tak aby można było ją zastosować w jak największej ilości
przypadków. Należy też rozważyć proceduralne generowanie, czyli potrzebę stworzenie nieskończonej mapy.\par
    Tutaj pomocne okaże się \emph{segmentowanie} (opisane w podrozdziale \ref{sec:chapter-01:chunking}), które pozwoli
na `wypiekanie' segmentów map. W tej pracy zostało założone, że mapa jest przestrzenią dwuwymiarową; jest to zarówno
wygodne, jak i poręczne, ponieważ większość rozwiązań używa takich map.

\newpage

\subsection{Segment minimalny} \label{subsubsec:chapter-02:idea:simulation:native-field}

W większości prostych rozwiązań wymagających algorytmu \texttt{A*}, zapisuje się najbardziej bazowe pole, jako kwadrat.
Ma to wiele zalet; można łatwo łączyć pola, jeżeli chcemy `postawić' budynek na mapie (większość budynków przypomina
prostokąty), łatwo tworzyć formacje jednostkami, albo sprawdzić kolizję. Z tego powodu, segmenty będą też
kwadratami (rys. \ref{img:chapter-02:idea:simulation:native-chunk}).

\begin{figure}[H]
    \centering
    \includegraphics[height=5cm]{../resources/chapter-02/images/native-chunk.png}
    \caption{Przykładowa wizualizacja jednostki minimalnej}
    \label{img:chapter-02:idea:simulation:native-chunk}
\end{figure}

\subsection{Segment} \label{subsubsec:chapter-02:idea:simulation:chunk}

Niezależnie od tego jaki rozmiar ma segment, powinien on wyglądać jak przeskalowany segment minimalny. Pozwala to na
wielopoziomowe nakładanie elementów na siebie, co jest szerzej opisane w podrozdziale \ref{subsec:Chapter-02:baking}. Na
rysunku \ref{img:chapter-02:idea:simulation:chunk} widać wizualizację segmentu dla wielkości \texttt{3}.

\begin{figure}[H]
    \centering
    \includegraphics[height=5cm]{../resources/chapter-02/images/chunk.png}
    \caption{Przykładowa wizualizacja segmentu}
    \label{img:chapter-02:idea:simulation:chunk}
\end{figure}

\subsection{Wykonanie}
Aby można było poruszać się płynnie po mapie, należy wyznaczyć miejsca, w których segmenty będą się łączyć. Każdy z
segmentów będzie ich używał jako wyjść (w przypadku poszukiwania ścieżki wewnątrz segmentu) lub jako wejść, jeżeli
będzie poszukiwał możliwego przejścia pomiędzy dwoma segmentami.

\begin{figure}[H]
    \centering
    \includegraphics[height=5cm]{../resources/chapter-02/images/example-chunk.png}
    \label{img:chapter-02:idea:execution:example-chunk}
    \caption{Przykładowa wizualizacja segmentu wielkości 5x5}
\end{figure}

W zwykłej implementacji, zakłada się dwa typy przejść – nieskośne oraz skośne. W wypadku segmentu wyznaczonego na
rysunku \ref{img:chapter-02:idea:execution:example-chunk}, trzeba znaleźć miejsca, w których mogłoby znaleźć się takie
przejście.

\begin{figure}[H]
    \centering
    \begin{tabular}{@{}ll@{}}
        a) & b) \\
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-exits.png} &
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-wasted-exits.png}
    \end{tabular}
    \caption{a) Przykładowa wizualizacja segmentu wielkości 5x5, b) Wizualizacja utraty przejść}
    \label{img:chapter-02:idea:execution:example-chunk-exits}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:execution:example-chunk-exits}.a, został przedstawiony najprostszy możliwy segment,
który ma osiem dostępnych punktów przejścia, które można użyć w trakcie podróżowania przez ten segment. Oczywiście,
powoduje to także utratę dokładności, co można zobaczyć na rysunku
\ref{img:chapter-02:idea:execution:example-chunk-exits}.b, jako pomarańczowe sektory. Algorytm nie będzie w stanie
znaleźć optymalnej ścieżki, jeżeli przebiega ona przez którykolwiek z pomarańczowych segmentów.\par
Jednak jeżeli porównanym to spostrzeżenie z opisem z podrozdziału \ref{subsec:chapter-02:idea}, można
zobaczyć podobieństwa. Kiedy wygenerujemy mapę `lokalną', używając segmentacji, będziemy w stanie stwierdzić jak
dotrzeć z dowolnego czarnego punktu, do innej znanej lokacji wyjściowej. W takim wypadku, jesteśmy w stanie wyliczyć
długość ścieżki jeszcze zanim znamy cel lub nawet początek naszej drogi! Pozostaje pytanie, jak zachować się w momencie
w którym, którekolwiek z wyjść jest niedostępne. Aby lepiej zrozumieć taką sytuację, należy przyjrzeć się rysunkowi
poniżej \ref{img:chapter-02:idea:execution:example-locking-exits}:

\begin{figure}[H]
    \centering
    \begin{tabular}{@{}ll@{}}
        a) & b) \\
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-avaiable-exits.png} &
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-blocked-exits.png}
    \end{tabular}
    \caption{a) Przykładowa wizualizacja dostępnych przejść, b) Wizualizacja niedostępnych przejść}
    \label{img:chapter-02:idea:execution:example-locking-exits}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:execution:example-locking-exits} zostały użyte trzy kolory; fioletowy symbolizuje
zablokowane przejście, czerwony zablokowane przejście, a zielony dostępne. W części
\ref{img:chapter-02:idea:execution:example-locking-exits}.a można zobaczyć typowy segment, który nie ma żadnych
blokujących czynników. Na rysunku \ref{img:chapter-02:idea:execution:example-locking-exits}.b, można zauważyć segment
umieszczony w rogu mapy (gdzie na lewo, czy w dół nie można dotrzeć). Aby algorytm działał wydajniej, wystarczy
zablokować dostęp do węzłów, które uważamy za zbędne lub aktualnie nieistotne. Pozwoli to algorytmowi na sprawdzanie
dystansu tylko do istniejących (dostępnych) ścieżek. Następnym problemem, który jest obecny w normalnej sytuacji, są
miejsca w które normalnie nie można `wejść'. Przykładowo tereny prywatne, oceany, góry itp. Na rysunku
\ref{img:chapter-02:idea:execution:example-obstruction} można zobaczyć przykład na segmencie:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-obstruction-disable.png}
    \caption{Przykładowa blokada niektórych obszarów segmentu}
    \label{img:chapter-02:idea:execution:example-obstruction}
\end{figure}

W tej sytuacji część przejść, przesunie się na najbliższy wolny segment, co pozwoli na zachowanie jak największej
dokładności. Jedyny problem jaki powstaje w ten sposób jest możliwość niezetknięcia się segmentów. Możliwym powodem jest
nagła zmiana kształtu blokady terenu na nowym segmencie. Niestety w pracy nie został ten problem rozwiązany i założeniem
jest, że taka sytuacja nie powinna nastąpić. Jednak by lepiej zrozumieć idee stojącą za systemem przejść, należy się
przyjrzeć rysunkowi \ref{img:chapter-02:idea:execution:example-chunk-bindings}:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-bindings.png}
    \caption{Opis przejść segmentu}
    \label{img:chapter-02:idea:execution:example-chunk-bindings}
\end{figure}

Rysunek \ref{img:chapter-02:idea:execution:example-chunk-bindings} prezentuje w jaki sposób są identyfikowane przejścia.
Pierwszą rzeczą, którą można zauważyć, jest oznaczone na rysunku dwanaście węzłów. Podczas gdy widoczne jest tylko
osiem, algorytm wykorzystuje ich dwanaście. Aby łatwiej zrozumieć, ile potrzeba węzłów do opisania najgorszego wypadku,
należy spojrzeć na rysunek poniżej (\ref{img:chapter-02:idea:execution:example-chunk-worst-case}):

\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{../resources/chapter-02/images/example-chunk-worst-case.png}
    \caption{Najgorszy wydajnościowo przypadek segmentu}
    \label{img:chapter-02:idea:execution:example-chunk-worst-case}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:execution:example-chunk-worst-case} można zobaczyć moment, w którym wszystkie
przejścia zostały wykorzystane (gdzie ciemnozielone oznaczenia, to niezmieniony stan w stosunku to wcześniejszych
rysunków). Dzięki takiemu rozwiązaniu, nie będzie sytuacji, w której przeszkoda rozdzieli segment na dwie części i
algorytm zignoruje to przejście.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-obstruction.png}
    \caption{Segment przedzielony na dwie równorzędne części}
    \label{img:chapter-02:idea:execution:example-chunk-split}
\end{figure}

Na rysunku (rys. \ref{img:chapter-02:idea:execution:example-chunk-split}) można zobaczyć sytuację, w której jeden
segment ma dwie niezależne od siebie części. Gdyby jego elementy były niepodzielne (nie poruszałyby się i byłoby ich
tylko osiem), można byłoby pominąć jedno optymalne przejście \texttt{7 -> 9} (widoczne na rys.
\ref{img:chapter-02:idea:execution:example-chunk-worst-case}). Istnieje też, jedna bardzo niewydajna sytuacja
zaprezentowana na rysunku poniżej:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-with-single-single-pice.png}
    \caption{Segment, który ma tylko jedno przejście dostępne}
    \label{img:chapter-02:idea:execution:single-pice}
\end{figure}

Widać (rys. \ref{img:chapter-02:idea:execution:single-pice}) tutaj nie ma żadnej dostępnej drogi, co
spowoduje sprawdzenie tego segmentu, po czym wykreślenie go z dostępnych (bez sprawdzenia żadnej ścieżki).

\subsection{Faza wypiekania} \label{subsec:Chapter-02:baking}

Zanim segment zostanie użyty, trzeba go wypalić. Wypalanie będzie polegało na zaplanowaniu, jak przejść z jednego
wejścia do innego i z jaką `ceną'. Wykorzystana zostanie bazowa implementacja algorytmu \texttt{A*}, która jest używana
w większości projektów\cite{a-star}. Ścieżki zostaną wyliczone kolejno, od węzła `0' do węzła 11, potem `1' do `11' itd.
Pełny rozpis widać poniżej:\newline

\begin{lstlisting}[caption={Node lookup}, label={lst:chapter-02:idea:baking:path-lookup}, captionpos=b, frame=single]
 0-> 1-> 2-> 3-> 4-> 5-> 6-> 7-> 8-> 9->10->11
 1-> 2-> 3-> 4-> 5-> 6-> 7-> 8-> 9->10->11
 2-> 3-> 4-> 5-> 6-> 7-> 8-> 9->10->11
 3-> 4-> 5-> 6-> 7-> 8-> 9->10->11
 4-> 5-> 6-> 7-> 8-> 9->10->11
 5-> 6-> 7-> 8-> 9->10->11
 6-> 7-> 8-> 9->10->11
 7-> 8-> 9->10->11
 8-> 9->10->11
 9->10->11
10->11
\end{lstlisting}

Na listingu \ref{lst:chapter-02:idea:baking:path-lookup} widać kolejne połączenia testowane przez program. W pierwszej
kolumnie sprawdzamy, jaki jest dystans do kolejnych (następujących po \texttt{->}) węzłów. Tutaj warte zauważenia jest,
że testowanie każdego przejścia pomija już wcześniej zbadane. Pozwala to na zmniejszenie użycia pamięci. Jeżeli jakieś
przejście jest niemożliwe, jego długość będzie zapisana jako \texttt{0}. W ten sposób, ustalamy optymalne przejścia dla
tego segmentu. Dodatkowo każdy segment, może zostać zgrupowany, aby stworzyć segment większego poziomu. Przykładowo na
rysunku poniżej \ref{img:chapter-02:idea:baking:chunk-leveling}:


\begin{figure}[H]
    \centering
    \begin{tabular}{@{}ll@{}}
        a) & b) \\
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-bigger-splitted.png} &
        \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/example-chunk-bigger.png}
    \end{tabular}
    \caption{a) Przykładowa wizualizacja segmentu złożonego z segmentów, b) Wizualizacja segmentu wyższego poziomu}
    \label{img:chapter-02:idea:baking:chunk-leveling}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:baking:chunk-leveling}.a widać 5x5 segmentów poziomu pierwszego. Każdy z nich,
składa się z dokładnie takich samych, przedstawionych na rysunku \ref{img:chapter-02:idea:baking:chunk-leveling}.b
segmentów poziomu zerowego (gdzie poziom zerowy reprezentuje segment minimalny na mapie). Aby stworzyć segment poziomu
drugiego, trzeba podobnie jak na rysunku \ref{img:chapter-02:idea:baking:chunk-leveling}.a, zgrupować segmenty poziomu
pierwszego, a aby stworzyć poziomu trzeciego, należy zgrupować segmenty poziomu drugiego.\par
Dzięki takiemu rozwiązaniu, segmenty można łączyć w nieskończoność, co pozwala na  rekursywne grupowaniem segmentów, co
pozwala na tworzenie niezwykle wydajnego systemu wypalonych ścieżek. Aby lepiej wyobrazić sobie jak duże będą mapy,
należy przyjrzeć się tabelce poniżej:

\begin{table}[h!]
    \centering
    \begin{tabular}{ || c | c | c || }

        \hline \hline
        Poziom & Wymiary & Ilość segmentów poziomu 0 \\
        \hline
        poziom 0  & 1 x 1                   & 1 \\
        poziom 1  & 5 x 5                   & 25 \\
        poziom 2  & 25 x 25                 & 625 \\
        poziom 3  & 125 x 125               & 15625 \\
        poziom 4  & 625 x 625               & 390625 \\
        poziom 5  & 3125 x 3125             & 9765625 \\
        poziom 6  & 15625 x 15625           & 244140625 \\
        poziom 7  & 78125 x 78125           & 6103515625 \\
        poziom 8  & 390625 x 390625         & 152587890625 \\
        poziom 9  & 1953125 x 1953125       & 3814697265625 \\
        poziom 10 & 9765625 x 9765625       & 95367431640625 \\
        poziom 11 & 48828125 x 48828125     & 2384185791015625 \\

        \hline \hline

    \end{tabular}
    \caption{Rozmiar mapy w stosunku do poziomu}
    \label{tbl:chapter-02:idea:baking:chunk-leveling}
\end{table}

\subsection{Szukanie używając wypieczonych segmentów}

Po przygotowaniu mapy, można wyznaczyć ścieżkę.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s1.png}
    \caption{Dwa punkty, na segmentu poziomu 2}
    \label{img:chapter-02:idea:baked-pf-1}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:baked-pf-1} wybrano dwa punkty, które będą nam służyły jako start (zielony punkt) i
cel (czerwony punkt). Algorytm, w momencie rozpoczęcia działania wybierze segment startowy i końcowy, po czym oba
zaznaczy (rys. \ref{img:chapter-02:idea:baked-pf-2}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s2.png}
    \caption{Oznaczone segmenty poziomu 1}
    \label{img:chapter-02:idea:baked-pf-2}
\end{figure}

Segment początkowy musi zostać oznaczony, aby zbadać wszystkie wyjścia które są dostępne. Pozwoli to utworzyć kolejkę
ich badania, zgodnie z ich odległością od celu. Końcowy segment jest zaznaczony, aby poznać moment w którym należy
algorytm zakończyć.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s3.png}
    \caption{Zaplanowana ścieżka z najkrótszego segmentu}
    \label{img:chapter-02:idea:baked-pf-3}
\end{figure}

Po zaznaczeniu odpowiednich segmentów oraz znalezieniu najbliższego wierzchołka. Algorytm zaczyna badać segment, w
którym węzłem \texttt{0} łączy się z węzłem \texttt{6} segmentu startowego (widoczne na rys.
\ref{img:chapter-02:idea:execution:example-chunk-worst-case}). Po znalezieniu takowego segmentu, możemy zbadać wszystkie
jego wyjścia, w poszukiwaniu kolejnego najbliższego węzła. Zachowanie to powtarza się, aż osiągniemy węzeł docelowy
(rysunek \ref{img:chapter-02:idea:baked-pf-4}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s4.png}
    \caption{Zaplanowanie ścieżki przez wypieczone segmenty}
    \label{img:chapter-02:idea:baked-pf-4}
\end{figure}

Na rysunku \ref{img:chapter-02:idea:baked-pf-4}, można zobaczyć żółte zaznaczenia. Pokazują one miejsca znane
algorytmowi, przez które przeszedł i zapamiętał je aby potem odtworzyć ścieżkę. Można porównać to zachowanie do
przykładu z początku rozdziału \ref{subsec:chapter-02:idea}:\newline

\texttt{ Legnica -> Prochowice -> Środa Śląska -> Wróblowice -> Wrocław}\newline
\texttt{ Legnica -> Szymanowice -> Budziszów W. -> Kąty Wrocławskie -> Wrocław }\newline

Jedyną różnicą, jest oznakowanie tych miejsc przez numery przejść. Po wyznaczeniu wszystkich pośrednich miejsc,
algorytm spróbuje dostać się do punktu wewnątrz segmentu docelowego. Jeżeli nie uda mu się, spróbuje wyznaczyć inny
segment który jest blisko celu.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s5.png}
    \caption{Wyznaczenie ścieżki do celu od aktualnego węzła}
    \label{img:chapter-02:idea:baked-pf-5}
\end{figure}

W tym przykładzie, udało mu się znaleźć ścieżkę. Widać ją na rysunku \ref{img:chapter-02:idea:baked-pf-5}. Kolejnym
krokiem jest odtworzenie ścieżki, korzystając z już wypieczonych wartości w segmentach przeszłych.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{../resources/chapter-02/images/path-finding/pf-s6.png}
    \caption{Odtworzenie ścieżki}
    \label{img:chapter-02:idea:baked-pf-6}
\end{figure}

\newpage

\subsection{Złożoność obliczeniowa}

Opisanie złożoności algorytmu jest trudne. Najgorszy przypadek złożoności dla standardowego algorytmu to:

\begin{equation}
    O(b^d),
\end{equation}

gdzie:
\begin{conditions}
 b     & współczynnik rozdzielności \\
 d     & głębokość rozwiązania (ilość węzłów w rozwiązaniu)
\end{conditions}

Dla zwykłej implementacji \texttt{A*} użytej w pracy, współczynnik \texttt{b} wynosi \texttt{8}. Dla algorytmu
wypiekanego jest to troszkę bardziej skomplikowane. Jeżeli segment nie jest blokowany w żadnym węźle, będzie on wynosił
\texttt{8}, jeżeli jest blokowany w prawie każdym węźle – \texttt{12}. Współczynnik rozgałęzienia będzie miał wpływ
głównie na niewielkie mapy (wielkość 5x5, 7x7), ponieważ tam długość rozwiązania jest niewielka.\par
Zmienna \texttt{d} w najgorszym wypadku zwykłego algorytmu reprezentuje długość rozwiązania. Algorytm segmentujący,
wykorzystujący wypalanie pozwala na ograniczenie ilości sprawdzanych węzłów, w zależności od poziomu badanego segmentu.
Wynika z tego następująca złożoność dla najgorszego przypadku:

\begin{equation}
    O(b^{\sqrt[L]{d}})
\end{equation}

gdzie:
\begin{conditions}
 b     & współczynnik rozdzielności \\
 d     & głębokość rozwiązania (ilość węzłów w rozwiązaniu) \\
 L     & poziom segmentu
\end{conditions}

Wynika z tego, że algorytm powinien przyśpieszać wykładniczo wraz ze wzrostem poziomu segmentu, w stosunku do
implementacji \texttt{A*} zaprezentowanej na listingu \ref{lst:chapter-01:a-star}. Na rysunku
\ref{img:chapter-02:complexity}, można zobaczyć wizualne porównanie złożoności obliczeniowej, dla kilku pierwszych
poziomów segmentu.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{../resources/chapter-02/images/complexity.png}
    \caption{Porównanie wizualne złożoności}
    \label{img:chapter-02:complexity}
\end{figure}

\section{Pseudokod}

\begin{lstlisting}[frame=single, style=base, caption={Pseudokod reprezentujący A*}, label={lst:chapter-02:a-star}, captionpos=b]

mapa := Segment badany przez algorytm; dowolny poziom

@function@ Baked_A_Star(start, cel, h):

// Należy sprawdzić, jaki jest aktualny poziom algorytmu.
// Jeżeli poziom jest równy jeden, należy użyć standardowego
// algorytmu A*
@if@ mapa.Poziom @not@ 1:
    alg := Baked_A_Star
@else@:
    alg := A_Star

@if@ mapa.Segment(start) == mapa.Segment(cel):
    // Jeżeli segment startowy, jest segmentem końcowym,
    // szukamy wewnętrznej ścieżki pomiędzy punktami.
    mapa := mapa.Segment(start)
    @return@ alg(start, cel, {heurystyka_wyboru})

// Zbiór dostępny, reprezentuje wszystkie niezbadane węzły.
// Na samym początku, zawiera tylko punkt startowy.
// Na początku zbioru są najmniejsze długości ścieżek.
zbior_dostepny := {start}

// Każdy węzeł, który jest tutaj zarejestrowany, posiada
// przyporządkowaną wartość, która mówi o poprzednim
// węźle na ścieżce najbliższej do celu. Mapa ta powinna
// też posiadać informacje o ścieżce która została wykonana.
poprzedni_wezel := pusta mapa

// Koszt przejścia od startu, do aktualnego węzła.
koszt_dojscia := mapa z wartością nieskończoności
// Początek wyliczania ścieżki
@for@ wyjscie @in@ mapa.Segment(start):
    // Znajdź i zapisz ścieżkę od start, do danego wyjścia.
    // Zapisz ją w węzłach do sprawdzenia.
    sciezka_do_wyjscia = alg(start, wyjscie, h)

    zbior_dostepny += wyjscie
    koszt_dojscia[wyjscie] := sciezka_do_wyjscia.dlugosc
    poprzedni_wezel[wyjscie] := mapa.Segment(start)

// Szacowany dystans do celu połączony z kosztem dojścia.
szacowany_dystans := mapa z wartością nieskończoności
szacowany_dystans[start] := h(start)

@while@ zbior_dostepny nie jest pusty:
    wezel_testowany :=
        węzeł ze zbior_dostepny, z najmniejszym
        szacowany_dystans[] od tego węzła

    @if@ mapa.Segment(wezel_testowany) == cel:
        sciezka := sciezka od wyjścia do celu
        sciezka += odtworz_sciezke_2(
            poprzedni_wezel,
            wezel_testowany)

        @return@ sciezka

    zbior_dostepny.Usun(wezel_testowany)
    @for@ wyjscie @in@ mapa.Segment(wezel_testowany)
        tymczasowy_koszt_przejscia :=
            Wypieczony koszt przejścia
            od wezel_testowany do wyjścia

        @if@ wyjscie @in@ zbior_dostepny:
            @continue@
        zbior_dostepny := wyjscie
        @if@ tymczasowy_koszt_przejscia < koszt_dojscia[wyjscie]:
            koszt_dojscia[wyjscie] :=
                tymczasowy_koszt_przejscia
            poprzedni_wezel[wyjscie] :=
                Wejście, które sąsiaduje z aktualnie wybranym
                wyjściem

// Jeżeli nie znaleziono ścieżki, nie ma sposobu, aby dojść
// do wybranego punktu
@return@ failure
\end{lstlisting}

\begin{lstlisting}[frame=single, style=base, caption={Pseudokod reprezentujący odtwarzani ścieżki.}, label={lst:chapter-01:reconstruct}, captionpos=b]
@function@ odtworz_sciezke_2(poprzedni_wezel, wezel)
    sciezka := {wezel}
    @while@ wezel @in@ poprzedni_wezel.Keys:
        sciezka += poprzedni_wezel[wezel].sciezka
    @return sciezka
\end{lstlisting}