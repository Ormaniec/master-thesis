\chapter{Wstęp}

Każdy człowiek w swoim życiu podróżuje. Niektórzy wykonują długie podróże, które zabierają ich dookoła świata, a inni
krótsze – do supermarketu. Niezależnie od wyboru, będą musieli odpowiedzieć sobie na pytanie ``Jak dojść do celu?''. W
tej pracy nie znajdzie się bezpośredniej odpowiedzi na to pytanie, a jedynie zostanie zaproponowane alternatywne
rozwiązanie, bardziej przypominające ludzki sposób nawigacji w przestrzeni.

\section{Poszukiwanie ścieżki}

Poszukiwaniem ścieżki można nazwać szereg operacji, których zadaniem jest zaplanowanie ścieżki pomiędzy dwoma wybranymi
punktami. Odpowiednio zaplanowana ścieżka, powinna zawsze zaczynać się w punkcie startowym i kończyć w punkcie, który
został oznaczony jako koniec. \par
Każdy algorytm poszukiwania ścieżki ma trzy bardzo ważne kryteria, które pozwalają ocenić jego przydatność. Pierwszym
jest długość ścieżki, która została zaplanowana. Długość ścieżki pomiędzy dwoma punktami powinna być jak najkrótsza,
chyba że użytkownik posiada inne oczekiwania (przykład: ścieżka musi dodatkowo zawierać pośredni punkt). Drugim
kryterium jest złożoność czasowa. Jeżeli czas analizy danego zagadnienia jest zbyt długi, to bardzo prawdopodobne jest,
że użytkownik zrezygnuje z czekania na wynik. Trzecim wymaganiem jest złożoność pamięciowa. Dotyczy ona głównie wypadków
zapamiętywania znanych rozwiązań, aby nie trzeba było ich planować wielokrotnie.

\section{Algorytm Dijkstry}

Jednym z najbardziej znanych algorytmów wyznaczania ścieżek; i bez tego na świecie jest algorytm Dijkstry. Mocną
stroną tego algorytmu jest znajdywanie zawsze najkrótszego rozwiązania (pierwsze kryterium). O wiele gorzej prezentuje
się w drugim kryterium, ponieważ wykonuje on przegląd zupełny dostępnych węzłów. Jednak jego największą zaletą jest
prostota oraz łatwość modyfikacji, poprzez dodawanie różnych heurystyk\ref{itm:dic:heuristic} do jego normalnej
implementacji. Jedną z jego najbardziej rozpowszechnionych adaptacji jest algorytm \texttt{A*}.

\newpage

\section{Algorytm A*}

    Algorytm \texttt{A*}\footnote{\url{https://en.wikipedia.org/wiki/A*_search_algorithm}}\cite{a-star} powstał na
potrzeby projektu `Shakey the Robot'\cite{shakey}\footnote{\url{https://en.wikipedia.org/wiki/Shakey_the_robot}}.
Projekt ten miał za zadanie stworzyć robota będącego w stanie samemu podejmować decyzje i reagować na instrukcje w
języku naturalnym (\emph{VUI}). W stosunku do wcześniejszych wersji robotów, które miały otrzymywać poszczególne kroki
osobno, `Shakey' mógł otrzymać całe zadanie i samemu je podzielić na mniejsze kroki.\par \texttt{A*} jest rozszerzeniem
algorytmu Dijkstry, a zmiana względem oryginału polega na badaniu tylko węzłów, które są najbliżej celu (według wybranej
heurystyki) oraz zakończeniu swojego działania w momencie odkrycia węzła końcowego. Dzięki tym zmianom, można zauważyć
znaczną poprawę złożoności obliczeniowej, niewielkim kosztem dokładności rozwiązania.\par
    Zastosowanie tego algorytmu w różnego typu aplikacjach (gry, mapy czy symulacje) pozwoliło na ulepszenie już
istniejących systemów, lub zwiększenie ich możliwości. Najczęściej słyszy się o tym algorytmie, kiedy szuka
się informacji dotyczących znajdywania ścieżek w grach. Algorytm ten pozwalał na tworzenie SI(AI), która mogła
nawigować po wirtualnym świecie w optymalny i błyskawicznie wyliczany sposób, bez potrzeby sprawdzania całej dostępnej
przestrzeni. Aktualnie ten algorytm jest standardem tworzenia SI przeciwników w grach, a nieliczne zmiany wprowadzane,
to nowe heurystyki.\par
    Największym problemem algorytmu \texttt{A*} w grach wideo, jest przeliczanie ścieżek dla dużej ilości jednostek, na
dużym obszarze w krótkim czasie. Problem ten można zauważyć w wielu grach \emph{RTS}. Zdarza się tam, że każdy gracz
mógł wydawać polecenia kilku tysiącom jednostek (przykładowo: \emph{Empire Earth 2}\footnote{\url{https://en.wikipedia.org/wiki/Empire_Earth_II}}). W każdej grze liczy się
maksymalna ilość klatek, które można wygenerować oraz szybkość odpowiedzi gry na sygnał wejściowy. Kiedyś stosowano
różne metody, aby ukryć to spowolnienie – od wypowiedzenia jednej z nagranych komend głosowych (`Zrozumiałem!',
`Wykonuje' czy `Przemieszczam się!'), które pozwalały na wyliczenie ścieżki oraz synchronizowanie stanu z innymi
graczami. Podczas gdy efekty definitywnie były zauważalne i satysfakcjonujące (Czas wypowiedzenia kwestii przeważnie
dawał przynajmniej sekundę na zaplanowanie ścieżki), nie sprawdziłby się na mapach proceduralnie generowanych (czyli
nieskończonych), gdzie mapa może osiągnąć spektakularne rozmiary.

\subsection{Złożoność obliczeniowa}

Złożoność czasowa algorytmu \texttt{A*} jest uzależniona od użytej heurystyki. W najgorszym wypadku może dojść do
przeglądu zupełnego. W takim wypadku będzie trzeba rozpatrywać następującą złożoność:

\renewcommand{\theequation}{\roman{equation}}

\newenvironment{conditions}
  {\par\vspace{\abovedisplayskip}\noindent\begin{tabular}{>{$}l<{$} @{${}={}$} l}}
  {\end{tabular}\par\vspace{\belowdisplayskip}}

\begin{equation}
    O(b^d)
\end{equation}

gdzie:
\begin{conditions}
 b     &  współczynnik rozdzielności \\
 d     & głębokość rozwiązania (ilość węzłów w rozwiązaniu)
\end{conditions}

Współczynnik rozgałęziania opisuje ilość węzłów do zbadania, które sąsiadują z obecnie badanym węzłem. Jest też możliwa
sytuacja, w której rozwiązanie jest niemożliwe, w takim wypadku najprostsza implementacja algorytmu nigdy się nie
zakończy.\par Najważniejszym czynnikiem wpływającym na złożoność algorytmu \texttt{A*} są heurystyki. Dobra heurystyka
pozwoli na zminimalizowanie ilości niepotrzebnych węzłów które zostały zbadane. Jeżeli heurystyka \texttt{h} spełnia
warunek

\newpage

\begin{equation}
    |\ h(x) - {h*}(x)\ | = O(log\ {h*}(x))
\end{equation}

gdzie:
\begin{conditions}
 h    & wybrana heurystyka \\
 h*   & optymalna heurystyka (dokładny dystans od punktu startowego do celu)
\end{conditions}

W takim wypadku \texttt{A*} musi badać wielomianowy przyrost wierzchołków, a nie przyrost wykładniczy. Na listingu
\ref{lst:chapter-01:a-star}, został zaprezentowany pseudokod, opisujący sposób działania algorytmu \texttt{A*}.

\subsection{Pseudokod}

\lstdefinestyle{base}{
  language=C,
  emptylines=1,
  breaklines=true,
  basicstyle=\ttfamily\color{black},
  moredelim=**[is][\color{blue}\itshape]{@}{@},
}

\begin{lstlisting}[frame=single, style=base, caption={Pseudokod reprezentujący A*}, label={lst:chapter-01:a-star}, captionpos=b]
@function@ A_Star(start, cel, h)
    // Zbiór dostępny, reprezentuje wszystkie niezbadane węzły.
    // Na samym początku, zawiera tylko podany węzeł.
    zbior_dostepny := {start}

    // Każdy węzeł, który jest tutaj zarejestrowany, posiada
    // przyporządkowaną wartość, która mówi o poprzednim
    // węźle na ścieżce najbliższej do celu.
    poprzedni_wezel := pusta mapa

    // Koszt przejścia od startu, do aktualnego węzła.
    koszt_dojscia := mapa z wartością nieskończoności
    koszt_dojscia[start] := 0

    // Szacowany dystans do celu połączony z kosztem dojścia.
    szacowany_dystans := mapa z wartością nieskończoności
    szacowany_dystans[start] := h(start)

    @while@ zbior_dostepny nie jest pusty:
        wezel_testowany :=
            węzeł ze zbior_dostepny, z najmniejszym
            szacowany_dystans[] od tego węzła

        @if@ wezel_testowany == cel:
            @return@ odtworz_sciezke(
                poprzedni_wezel,
                wezel_testowany)

        zbior_dostepny.Usun(wezel_testowany)
        @for@ @each@ sasiad @of@ wezel_testowany:
            potencjalny_koszt_dojscia :=
                koszt_dojscia[wezel_testowany]
                + koszt_przejscia

            @if@ potencjalny_koszt_dojscia < koszt_dojscia[wezel_testowany]:
                // Ścieżka jest lepsza od poprzedniej znanej.
                poprzedni_wezel[sasiad] := current
                koszt_dojscia[sasiad] := potencjalny_koszt_dojscia
                szacowany_dystans[sasiad] :=
                    koszt_dojscia[sasiad] + h(sasiad)

                @if@ sasiad @not in@ openSet
                    openSet.add(sasiad)
    @return@ failure
\end{lstlisting}

\begin{lstlisting}[frame=single, style=base, caption={Pseudokod reprezentujący odtwarzani ścieżki.}, label={lst:chapter-01:reconstruct}, captionpos=b]
@function@ odtworz_sciezke(poprzedni_wezel, wezel)
    sciezka := {wezel}
    @while@ wezel @in@ poprzedni_wezel.Keys:
        sciezka += poprzedni_wezel[wezel]
    return sciezka
\end{lstlisting}

\section{Wypiekanie} \label{sec:chapter-01:baking}

    Zanim zastosowano \texttt{A*} w grach platformowych\cite{platformer}, poruszanie się większości przeciwników,
polegało na prostym sprawdzeniu, czy przed postacią jest blokada. Jeśli nie było przeszkody, trasa była kontynuowana,
jeżeli wykryto kolizje, postać kierowała się w przeciwnym kierunku. Wraz ze wzrostem mocy obliczeniowej komputerów,
zaczęto coraz bardziej rozwijać SI przeciwników. To doprowadziło do powstania jednej z najczęściej używanych metod,
`oskryptowanej sekwencji'\footnote{\url{https://pl.wikipedia.org/wiki/Oskryptowana_sekwencja}}. Polegała ona na
wcześniejszym przygotowaniu ścieżki, którą miały pokonać postacie oraz potencjalnych akcji, które były wykonywane w
trakcie jej trwania. Dawało to graczowi wrażenie, że bierze udział w scenie podobnej do filmu, co zwiększało możliwości
narracyjne gier.\par

Kolejnym krokiem pozwalającym na bardzo tanie przygotowanie ścieżek, było `wypieczenie' ścieżek (znane także jako
wypalanie; (ang.) \emph{Path baking}). Metoda polegała na wcześniejszym przygotowaniu gotowych ścieżek, które były
dostępne dla SI, aby poruszać się po symulowanym terenie, bez kolizji z przedmiotami. Olbrzymią zaletą tego rozwiązania
była wolność, jaką twórcy gier otrzymali przy projektowaniu postaci, ponieważ ścieżka nie była powiązana bezpośrednio z
postacią – jak we wcześniej wspomnianych sekwencjach – ale obiekty były przypisywane do ścieżki. Jest jednak problem z
metodą wypiekania. Kiedy ścieżki zostały już przygotowane, każdy element `mapy' który nie był rozważony przy wypiekaniu,
jest zupełnie nieosiągalny dla SI. Dobrym przykładem jest \emph{Counter Strike : Global
offensive}\footnote{\url{https://blog.counter-strike.net/}}\
\footnote{\url{https://www.reddit.com/r/csmapmakers/comments/4b6vwl/csgo_problem_with_bots_navigation_mesh/}}, w którym
każda mapa posiada wypieczoną sieć połączeń, nazywaną `mesh'. Kiedy zmienia się układ mapy, trzeba na nowo rozłożyć
siatkę, aby przeciwnicy sterowani przez SI byli w stanie nawigować po niej. Wystąpił brak synchronizacji pomiędzy mapą a
siatką, powodując nierozsądne zachowania SI.

\section{Segmentowanie} \label{sec:chapter-01:chunking}

Segmentowanie polega na podziale dużych elementów, na mniejsze. Dobrym przykładem segmentowania jest remont drogi. Nigdy
nie zamyka się głównej drogi w całości, aby załatać klika niewielkich dziur na jezdni. Przeważnie wybiera się
niewielką część jezdni, a po zakończeniu pracy nad jedną niedoskonałością, zamyka się inny fragment (zwalniając dostęp
do tej, w której prace są skończone).\par
Jednym z lepszych przykładów zastosowania segmentowania w oprogramowaniu jest bardzo popularna gra komputerowa
`Minecraft'\footnote{\url{https://minecraft.gamepedia.com/Chunk}}. Jest ona szczególnie dobrym porównaniem, ponieważ
mapa w grze jest proceduralnie generowana. Świetnie to odzwierciedla problem poszukiwania drogi w prawdziwym świecie,
ponieważ mapa może osiągnąć bardzo duże wielkości. Dzięki segmentacji można podzielić pracę na niezależne od siebie
zadania i zmniejszyć czas oczekiwania na pewne przeliczenia, co z kolei pozwoli na zwiększenie liczby klatek na sekundę
w grze. Dodatkową zaletą segmentacji w tym bardzo konkretnym przypadku jest możliwość wyłączenia części kalkulacji,
kiedy postać użytkownika na mapie, znajdzie się zbyt daleko daleko od segmentu, aby istotnie zauważyć efekt.

\section{inspiracja}

W grach bardzo często ogranicza się rozmiar mapy do kilku kilometrów. Pozwala to na symulowanie wielu sytuacji i w
większości przypadków jest to wystarczający rozmiar. Jednak zastanawiające jest, w jaki sposób człowiek używając mapy,
może tak szybko oszacować trasę z jednej półkuli na inną. Każda osoba, kiedy rozważa trasę, nie próbuje jej wyznaczyć w
pełni. Staramy się wyznaczyć najpierw punkty, które znamy – kościół, supermarket, miasto, kraj – i wiemy jak się tam
dostać oraz ile czasu to nam zabierze. Następnie zaczynamy rozważać trasę z miejsca, które znamy do naszego celu – jak
spod kościoła dojść do \emph{ul. Skórniczej 131}.\par W takim wypadku czy udało by się symulować taki sposób myślenia w
maszynie?
