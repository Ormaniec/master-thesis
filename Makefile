pdf: tex/master-thesis.tex
	
	TEXINPUTS=tex/:resources/:${TEXINPUTS} pdflatex -output-directory="./out" tex/master-thesis.tex

pdf-bib: out/master-thesis.aux doc.bib
	TEXINPUTS=tex/:resources/:${TEXINPUTS} pdflatex -output-directory="./out" tex/master-thesis.tex
	TEXMFOUTPUT="out:" bibtex out/master-thesis
	TEXINPUTS=tex/:resources/:${TEXINPUTS} pdflatex -output-directory="./out" tex/master-thesis.tex
	TEXINPUTS=tex/:resources/:${TEXINPUTS} pdflatex -output-directory="./out" tex/master-thesis.tex